const { sequelize } = require("..");
const { Sequelize } = require("sequelize");
const Student = require("./student.model");

class Group extends Sequelize.Model {}

Group.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "group" }
);

Group.hasMany(Student);

Student.belongsTo(Group, {
  foreignKey: "groupId",
});

module.exports = Group;
