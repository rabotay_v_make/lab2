const { initDB } = require("./database/config");
const Group = require("./database/models/group.model");
const Student = require("./database/models/student.model");

async function bootstrap() {
  await initDB();
  const groupList = await Group.findAll({
    include: [
      {
        model: Student,
      },
    ],
  });

  console.log(groupList);
}

bootstrap();
